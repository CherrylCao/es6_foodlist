export default class Food {
  constructor(ma, ten, loai, gia, khuyenMai, tinhTrang, hinhMon, moTa) {
    this.id = ma;
    this.name = ten;
    this.type = loai == "loai1" ? "Chay" : "Man";
    this.price = gia;
    this.discount = khuyenMai;
    this.status = tinhTrang == "0" ? "Hết" : "Còn";
    this.image = hinhMon;
    this.desc = moTa;
  }
  tinhGiaKM = function () {
    return this.price * (1 - this.discount);
  };
}

export let showThongTin = (data) => {
  let { id, name, type, price, discount, status, image, desc } = data;
  document.getElementById("spMa").innerText = id;
  document.getElementById("spLoaiMon").innerText = type;
  document.getElementById("spTenMon").innerText = name;
  document.getElementById("spGia").innerText = price;
  document.getElementById("spKM").innerText = discount;
  document.getElementById("spGiaKM").innerText = data.tinhGiaKM();
  document.getElementById("spTT").innerText = status;
  document.getElementById("pMoTa").innerText = desc;
  document.getElementById("imgMonAn").src = image;
};
