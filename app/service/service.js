const BASE_URL = "https://64c62b61c853c26efadb28fe.mockapi.io/FoodList";

let getList = () => {
  return axios({
    url: BASE_URL,
    method: "GET",
  });
};

let deleteFood = (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "DELETE",
  });
};

let addFood = (food) => {
  return axios({
    url: BASE_URL,
    method: "POST",
    data: food,
  });
};

let getDetail = (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "GET",
  });
};

let updateFood = (id) => {
  return axios({
    url: `${BASE_URL}/${id}`,
    method: "PUT",
  });
};

let foodServ = {
  getList,
  deleteFood,
  addFood,
  getDetail,
  updateFood,
};

export default foodServ;
